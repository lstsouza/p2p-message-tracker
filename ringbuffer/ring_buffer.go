package ringbuffer

import (
	"errors"
	"container/list"
)

type RingBuffer interface {
	// Returns an ordered slice representing the data in the buffer
	GetAll() ([]string)
	// Pushes a new element to the next position in the buffer. If an element is overwritten, return it
	Push(elem string) (string)
	// Deletes provided element and rebalances buffer
	Delete(elem string) (error)
	// Get message count
	Count() (int)
	// Returns a pointer to the internal list. 
	// This allows the internal list to be manipulated directly. Should be used with care
	GetList() (*list.List)
}

type RingBufferImpl struct {
	buf    *list.List
	cap	   int
}

func New(size int) RingBuffer {
	return &RingBufferImpl {
		buf: 	list.New(),
		cap:    size,
	}
}

func (rb* RingBufferImpl) GetAll() ([]string) {
	order := make([]string, rb.Count())
	var index int
	// Get all elements in order, from oldest to newest
	for e := rb.buf.Front(); e != nil; e = e.Next() {
		order[index] = e.Value.(string)
		index++
	}
	return order
}

func (rb* RingBufferImpl) GetList() (*list.List) {
	return rb.buf
}

func (rb* RingBufferImpl) Push(elem string) (string) {
	rb.buf.PushBack(elem)
	var overwritten string
	// If list exceeds configured capacity, delete oldest entry
	if rb.Count() > rb.cap {
		overwritten, _ = rb.buf.Remove(rb.buf.Front()).(string)
	}
	return overwritten
}

var ErrElementNotFound = errors.New("Ring Buffer: Element not found")

func (rb* RingBufferImpl) Delete(elem string) (error) {
	for e := rb.buf.Front(); e != nil; e = e.Next() {
		if e.Value == elem {
			rb.buf.Remove(e)
			return nil
		}
	}
	return ErrElementNotFound
}

func (rb* RingBufferImpl) Count() (int) {
	return rb.buf.Len()
}
