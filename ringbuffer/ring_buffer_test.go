package ringbuffer_test

import (
    "testing"
	"fmt"
	"github.com/stretchr/testify/assert"
	"gitlab.com/lstsouza/p2p-msg-tracker/ringbuffer"
)

func createStringElement(num int) (string) {
	return fmt.Sprintf("RingBufferElement%d", num)
}

// Helper function to initialize a ring buffer with specified values for ease of testing
func initRingBuffer(capacity int, numElem int) ringbuffer.RingBuffer {
    rb := ringbuffer.New(capacity)
    for i := 0; i < numElem; i++ {
        rb.Push(createStringElement(i))
    }
    return rb
}

func TestRingBuffer_Push(t *testing.T) {
	t.Run("add, overflow capacity, get all, get list", func(t *testing.T) {
		cap := 3
		rb := initRingBuffer(cap, 0)

		overritten := rb.Push(createStringElement(0))
		assert.Equal(t, rb.Count(), 1)
		assert.Equal(t, overritten, "")
		assert.Equal(t, rb.GetAll(), []string{
			createStringElement(0),
		})

		overritten = rb.Push(createStringElement(1))
		assert.Equal(t, overritten, "")
		overritten = rb.Push(createStringElement(2))
		assert.Equal(t, rb.Count(), 3)
		assert.Equal(t, overritten, "")

		overritten = rb.Push(createStringElement(3))
		assert.Equal(t, rb.Count(), 3)
		assert.Equal(t, overritten, createStringElement(0))
		assert.Equal(t, rb.GetAll(), []string{
			createStringElement(1),
			createStringElement(2),
			createStringElement(3),
		})

		overritten = rb.Push(createStringElement(4))
		assert.Equal(t, rb.Count(), 3)
		assert.Equal(t, createStringElement(1), overritten)
		assert.Equal(t, rb.GetAll(), []string{
			createStringElement(2),
			createStringElement(3),
			createStringElement(4),
		})
	})
}

func TestRingBuffer_Delete(t *testing.T) {
	t.Run("delete existent, delete non existent", func(t *testing.T) {
		cap := 3
		rb := initRingBuffer(cap, cap)
		assert.Equal(t, rb.GetAll(), []string{
			createStringElement(0),
			createStringElement(1),
			createStringElement(2),
		})

		err := rb.Delete(createStringElement(1))
		assert.NoError(t, err)
		assert.Equal(t, rb.GetAll(), []string{
			createStringElement(0),
			createStringElement(2),
		})

		err = rb.Delete(createStringElement(1))
		assert.ErrorIs(t, err, ringbuffer.ErrElementNotFound)
		assert.Equal(t, rb.GetAll(), []string{
			createStringElement(0),
			createStringElement(2),
		})
	})
}