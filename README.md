# P2P Message Tracker

## Overview
This repository contains the code of the MessageTracker component to be used in a peer-to-peer application. The MessageTracker ensures efficient message handling by tracking messages in FIFO order and preventing duplication.

## Authorship
- I am not the autor of the tests present in `network/message_tracker_test.go`, with the exception of `TestMessageTrackerConcurrency`. All other tests present in this file were provided as basic functionality conformance criteria.
- `Message` struct and `MessageTracker` interface were externally provided by other people and could not be changed
- The signature for the constructor function `NewMessageTracker` was also externally provided functionality conformance criteria, although the constructor logic itself was blank.
- Everything else was written by myself (Leonardo Souza)

## Goals
Performance is a critical point to be addressed by the MessageTracker, one it is a central part of the application and we can expect it to be one of the most used components in the application it is part of. As such, the following objectives were pursued during development:
- Optimize Memory Access and Computation: Ensure, whenever possible, that messages can be managed in a fast way, avoiding unnecessary memory accesses and minimizing the need for copies, resizes, iterations through arrays, garbage collection and others. Also, take advantage of caching algorithms to speed-up execution.
- Scalability: Handle an increasing amount of messages minimizing degradation in performance. It is also important to ensure the application is thread-safe, allowing safe handling of race-conditions.

## Design and Technical Choices
The MessageTracker implementation has 3 (three) components;
- messages: This is a hash map which stores all Message instances currently being tracked. Implementing it as such, allows the program to access stored messages or add new ones in O(1) time. 
- order: This is a data structure used to store the order in which the messages were received. It is basically an ordered list of Message.IDs. To implement this, a new interface called RingBuffer was implemented. RingBuffer is implemented by using Go’s linked list implementation with added constraints, like a maximum capacity and allowing only strings to be included in the list through its interface. Implementing it using a double-linked list allows for O(1) insertion of an entry at the end of the list, as well as O(1) deletion of the oldest entry. Also, it is possible to delete an entry somewhere in the middle of the list without the need for a reorder or rebalance afterwards. Every deletion is Ω(1), O(n). The downside is that processor caching algorithms tend to be less efficient with lists (when compared to arrays) because adjacent elements in the list are not necessarily in adjacent memory addresses. However, avoiding rebalancing the whole buffer after a deletion makes up for the loss caching performance.  
- mutex: A standard Go read-write mutex (RWMutex) to allow the interface to safely run in multithreaded environments.

## Technical Performance Analysis
- MessageTracker.Add - O(1): Add is done in 3 (three) steps. First, the Message itself is stored in the MessageTrackerImpl.messages map. After that, Message.ID is stored at the tail of MessageTrackerImpl.order to keep the correct sequence. Finally, in case the configured capacity is exceeded, the oldest message is excluded from both MessageTrackerImpl.order and MessageTrackerImpl.messages. All of the described steps have O(1) complexity. 
- MessageTracker.Delete - O(n): Delete is done in 2(two) steps. First, the given Message.ID is excluded from MessageTrackerImpl.messages map, which is a simple O(1) operation. The less efficient part, however, has to do with maintaining MessageTrackerImpl.order updated. The order update process has complexity ranging from Ω(1) to O(n), with the best case being deletion of the oldest (the most common use case) message, and the worst case being deletion of the newest message.
- MessageTracker.Message - O(1): this is a simple lookup in MessageTrackerImpl.messages. Because a hash map was used for storing the Message, the complexity for this operation is O(1)
- MessageTracker.Messages - O(n): This operation involves iterating through the entire MessageTrackerImpl.order list and copying the related Message information, in the correct order, to an array.

## Tests and Benchmarks
The tests can be wasily run on Linux by running `$go test ./network` and `$go test ./ringbuffer` on shell. To run the benchmark, run `$go test -bench=. ./network` on shell. 

In the benchmark case, please note that in the provided result of X ns/op, each operation (op) is actually a group of 10000 (ten thousand) operations by default. For example, in the test `BenchmarkAdd/add_when_not_exceeding_maximum_capacity-32` below, the result says each “operation” took 1448749 ns, which means that each iteration actually took around 145 ns to execute.

```
goos: linux
goarch: amd64
pkg: gitlab.com/lstsouza/p2p-msg-tracker/network
cpu: 13th Gen Intel(R) Core(TM) i9-13900HX
BenchmarkAdd/add_when_not_exceeding_maximum_capacity-32                      818           1448749 ns/op
BenchmarkAdd/add_when_already_exceeding_maximum_capacity-32                  876           1382514 ns/op
BenchmarkDelete/delete_from_head_-_best_case_scenario-32                    2722            435160 ns/op
BenchmarkDelete/delete_from_tail_-_worst_case_scenario-32                      8         140395045 ns/op
BenchmarkMessages-32                                                        5512            202794 ns/op
PASS
ok      gitlab.com/lstsouza/p2p-msg-tracker/network     22.139s
``` 
