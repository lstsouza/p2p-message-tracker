package network

import (
	"errors"
	"sync"
	"gitlab.com/lstsouza/p2p-msg-tracker/ringbuffer"
)

// MessageTracker tracks a configurable fixed amount of messages.
// Messages are stored first-in-first-out.  Duplicate messages should not be stored in the queue.
type MessageTracker interface {
	// Add will add a message to the tracker, deleting the oldest message if necessary
	Add(message *Message) (err error)
	// Delete will delete message from tracker
	Delete(id string) (err error)
	// Get returns a message for a given ID.  Message is retained in tracker
	Message(id string) (message *Message, err error)
	// Messages returns messages in FIFO order
	Messages() (messages []*Message)
}

// ErrMessageNotFound is an error returned by MessageTracker when a message with specified id is not found
var ErrMessageNotFound  = errors.New("message not found")

// ErrOrderAndMapMismatch is an error returned by MessageTracker when the order buffer shows a message not found in hash map or vice versa
var ErrOrderAndMapMismatch = errors.New("mismatch detected between order buffer and map")

type MessageTrackerImpl struct {
	messages map[string]*Message 	// Hash Map for fast look-up of messages
	order    ringbuffer.RingBuffer  // Ring Buffer to store message order
	mutex    sync.RWMutex			// Protect read and write access to internal data structures
}

func NewMessageTracker(length int) MessageTracker {
	return &MessageTrackerImpl{
		messages: make(map[string]*Message),
		order:    ringbuffer.New(length),
	}
}

func (mt *MessageTrackerImpl) Add(message *Message) error {
	var retError error
	mt.mutex.Lock()
	defer mt.mutex.Unlock()
	if _, exists := mt.messages[message.ID]; !exists {
		// Message is not a duplicate. Store it in map
		mt.messages[message.ID] = message

		// Update message order
		overwritten := mt.order.Push(message.ID)
		if overwritten != "" {
			// The oldest message id was overritten because buffer capacity was exceded
			// Delete corresponding message from map
			if _, exists := mt.messages[overwritten]; exists {
				delete(mt.messages, overwritten)
			} else {
				retError = ErrOrderAndMapMismatch
			}
		}
	}
	return retError
}

func (mt *MessageTrackerImpl) Delete(id string) error {
	var retError error
	mt.mutex.Lock()
	defer mt.mutex.Unlock()
	if _, exists := mt.messages[id]; exists {
		// Message exists. Delete it from map
		delete(mt.messages, id)
		// Remove corresponding message ID from order buffer
		if mt.order.Delete(id) != nil {
			retError = ErrOrderAndMapMismatch
		}
	} else {
		retError = ErrMessageNotFound
	}
	return retError
}

func (mt *MessageTrackerImpl) Message(id string) (*Message, error) {
	var retMsg *Message
	var retError error
	mt.mutex.RLock()
	defer mt.mutex.RUnlock()
	if msg, exists := mt.messages[id]; exists {
		retMsg = msg
	} else {
		retError = ErrMessageNotFound
	}
	return retMsg, retError
}

func (mt *MessageTrackerImpl) Messages() []*Message {
	ids := mt.order.GetList()
	var index int
	
	// Iterate over ordered IDs list and fetch corresponding messages
	mt.mutex.RLock()
	defer mt.mutex.RUnlock()
	messages := make([]*Message, ids.Len())
	for e := ids.Front(); e != nil; e = e.Next() {
		if message, exists := mt.messages[e.Value.(string)]; exists {
			messages[index] = message
			index++
		}
	}
	return messages
}
