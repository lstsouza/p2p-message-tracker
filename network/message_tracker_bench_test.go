package network_test

import (
    "testing"
	"gitlab.com/lstsouza/p2p-msg-tracker/network"
)

const DEFAULT_MSG_TRACKER_CAP int = 10000

func setupMessageTracker(cap int) (network.MessageTracker, []*network.Message) {
	mt := network.NewMessageTracker(cap)
	for i := 0; i < cap; i++ {
		message := generateMessage(i)
		mt.Add(message)
	}
	messages := mt.Messages()
	return mt, messages
}

func BenchmarkAdd(b *testing.B) {
	b.Run("add when not exceeding maximum capacity", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			b.StopTimer()
			_, messages := setupMessageTracker(DEFAULT_MSG_TRACKER_CAP)
			mt := network.NewMessageTracker(DEFAULT_MSG_TRACKER_CAP)
			b.StartTimer()

			for _, m := range messages {
				mt.Add(m)
			}
		}
	})

	b.Run("add when already exceeding maximum capacity", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			b.StopTimer()
			mt, _ := setupMessageTracker(DEFAULT_MSG_TRACKER_CAP)
			messages := make([]*network.Message, DEFAULT_MSG_TRACKER_CAP)
			for i := 0; i < DEFAULT_MSG_TRACKER_CAP; i++ {
				messages[i] = generateMessage(DEFAULT_MSG_TRACKER_CAP + i)
			}
			b.StartTimer()
			
			for _, m := range messages {
				mt.Add(m)
			}
		}
	})
}

func BenchmarkDelete(b *testing.B) {
	b.Run("delete from head - best case scenario", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			b.StopTimer()
			mt, messages := setupMessageTracker(DEFAULT_MSG_TRACKER_CAP)
			b.StartTimer()
			// Delete messages - always in head
			for _, m := range messages {
				mt.Delete(m.ID)
			}
		}
	})

	b.Run("delete from tail - worst case scenario", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			b.StopTimer()
			mt, messages := setupMessageTracker(DEFAULT_MSG_TRACKER_CAP)
			lastIndex := len(messages) - 1
			b.StartTimer()
			// Delete messages - always in tail
			for j, _ := range messages {
				mt.Delete(messages[lastIndex - j].ID)
			}
		}
	})
}

func BenchmarkMessages(b *testing.B) {
	mt, _ := setupMessageTracker(DEFAULT_MSG_TRACKER_CAP)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		mt.Messages()
	}
}

